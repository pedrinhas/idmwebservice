﻿// Decompiled with JetBrains decompiler
// Type: IDMWebService.IDMService
// Assembly: IDMWebService, Version=1.0.3299.20037, Culture=neutral, PublicKeyToken=null
// MVID: ACEF342D-9388-4D48-87E3-3A93C0ED0718
// Assembly location: A:\Git repositories\idmwebservice\IIS\bin\IDMWebService.dll

using DataProtection;
using SPML;
using System;
using System.ComponentModel;
using System.Configuration;
using System.IO;
using System.Text;
using System.Web.Services;
using System.Xml;

namespace IDMWebServiceDescompilado
{
  [WebService(Namespace = "https://intranet.utad.pt/wsIDM/")]
  public class IDMService : WebService
  {
    private IContainer components = (IContainer) null;
    protected string _idm_host;
    protected string _idm_login;
    protected string _idm_password;

    public IDMService()
    {
      this.InitializeComponent();
      this._idm_host = IDMService.getKeyValue("idm/host", true);
      this._idm_login = IDMService.getKeyValue("idm/login", true);
      this._idm_password = IDMService.getKeyValue("idm/password", true);
    }

    private void InitializeComponent()
    {
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    [WebMethod(Description = "Change User Password", EnableSession = true)]
    public string changeuserpassword(string account, string password)
    {
      string data = "";
      spmlService spmlService = new spmlService(this._idm_host, this._idm_login, this._idm_password);
      string str;
      try
      {
        str = !spmlService.SPMLLogin() ? spmlService.SPMLError : (!spmlService.SPMLChangeUserPassword(account, password) ? spmlService.SPMLError : spmlService.SPMLResult);
      }
      catch
      {
        str = spmlService.SPMLError;
      }
      finally
      {
        str = this.returnData(spmlService.SPMLResult, spmlService.SPMLErrorCod, spmlService.SPMLError, data);
        spmlService.SPMLLogout();
      }
      return str;
    }

    [WebMethod(Description = "Reset User Password", EnableSession = true)]
    public string resetuserpassword(string account)
    {
      string data = "";
      spmlService spmlService = new spmlService(this._idm_host, this._idm_login, this._idm_password);
      string str;
      try
      {
        if (spmlService.SPMLLogin())
        {
          if (spmlService.SPMLResetUserPassword(account))
          {
            str = spmlService.SPMLResult;
            if (spmlService.SPMLResult.Equals(spmlService.SPLMResults.success.ToString()))
            {
              data += "<DATA>";
              data = data + "<NEWPASSWORD>" + spmlService.SPMLNewUserPassword + "</NEWPASSWORD>";
              data += "</DATA>";
            }
          }
          else
            str = spmlService.SPMLError;
        }
        else
          str = spmlService.SPMLError;
      }
      catch
      {
        str = spmlService.SPMLError;
      }
      finally
      {
        str = this.returnData(spmlService.SPMLResult, spmlService.SPMLErrorCod, spmlService.SPMLError, data);
        spmlService.SPMLLogout();
      }
      return str;
    }

    [WebMethod(Description = "Reset User Password", EnableSession = true)]
    public string modifyuserdisplayname(string account, string displayname)
    {
      string data = "";
      spmlService spmlService = new spmlService(this._idm_host, this._idm_login, this._idm_password);
      string str;
      try
      {
        str = !spmlService.SPMLLogin() ? spmlService.SPMLError : (!spmlService.modifyRequestDisplayName(account, displayname) ? spmlService.SPMLError : spmlService.SPMLResult);
      }
      catch
      {
        str = spmlService.SPMLError;
      }
      finally
      {
        str = this.returnData(spmlService.SPMLResult, spmlService.SPMLErrorCod, spmlService.SPMLError, data);
        spmlService.SPMLLogout();
      }
      return str;
    }

    private string returnData(string result, string errorcod, string errordesc, string data)
    {
      string str = "" + "<IDMUTAD>" + "<RESULT>" + result + "</RESULT>";
      if (result.Equals(spmlService.SPLMResults.failure.ToString()))
        str = str + "<ERROR>" + "<CODE>" + errorcod + "</CODE>" + "<DESCRIPTION>" + errordesc + "</DESCRIPTION>" + "</ERROR>";
      else if (!data.Equals(""))
        str += data;
      return str + "</IDMUTAD>";
    }

    public static string getKeyValue(string sKey, bool bdecrypt)
    {
      XmlDocument xmlDocument = new XmlDocument();
      string EncryptString;
      try
      {
        string str = ConfigurationSettings.AppSettings["xmlconfigparam"];
        if (!File.Exists(str))
          throw new ArgumentNullException("Ficheiro de Connecção (XML) não existe!");
        xmlDocument.Load(str);
        EncryptString = new XmlNodeReader(xmlDocument.SelectSingleNode("config/" + sKey)).ReadElementString();
        if (bdecrypt)
          EncryptString = IDMService.DecryptString(EncryptString);
      }
      catch (Exception ex)
      {
        throw new ArgumentNullException("Erro ao ler a string com a Path dos anexos - " + sKey + " - " + ex.Message);
      }
      return EncryptString;
    }

    public static string DecryptString(string EncryptString)
    {
      DataProtector dataProtector = new DataProtector(DataProtector.Store.USE_MACHINE_STORE);
      try
      {
        byte[] cipherText = Convert.FromBase64String(EncryptString);
        return Encoding.ASCII.GetString(dataProtector.Decrypt(cipherText, (byte[]) null));
      }
      catch (Exception ex)
      {
        throw new ArgumentNullException("Erro ao decriptar a string de connecção a DB!" + " - " + ex.Message);
      }
    }
  }
}
