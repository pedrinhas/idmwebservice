﻿// Decompiled with JetBrains decompiler
// Type: SPML.spmlService
// Assembly: SPML, Version=1.0.3455.27787, Culture=neutral, PublicKeyToken=null
// MVID: 2C634600-53AA-4151-8958-4D2F858E172C
// Assembly location: A:\Git repositories\idmwebservice\IIS\bin\SPML.dll

using System;
using System.IO;
using System.Net;
using System.Text;
using System.Xml;

namespace SPML
{
  public class spmlService
  {
    protected string _ERROR_SPMLDEFAULT_COD = "0000";
    protected string _ERROR_SPMLLOGIN_COD = "0001";
    protected string _ERROR_SPMLLOGOUT_COD = "0002";
    protected string _ERROR_SPMLCHGUSERPWD_COD = "0003";
    protected string _ERROR_SPMLRSTUSERPWD_COD = "0004";
    protected string _ERROR_SPMLUSERNOTEXIST_COD = "0005";
    protected string _ERROR_SPMLPWDHISTORY_COD = "0006";
    protected string _ERROR_SPMLPWDVIOLATION_COD = "0007";
    protected string _ERROR_SPMLCHGUSERDATA_COD = "0008";
    protected string _ERROR_SPMLLOGIN = "Erro no pedido do token de autenticação no IDM";
    protected string _ERROR_SPMLLOGOUT = "Erro no pedido do destruição do token de autenticação no IDM";
    protected string _ERROR_SPMLCHGUSERPWD = "Erro na alteração da password da conta";
    protected string _ERROR_SPMLRSTUSERPWD = "Erro no reset da password da conta";
    protected string _ERROR_SPMLCHGUSERDATA = "Erro na alteração dos da conta";
    protected string _ERROR_SPMLUSERNOTEXIST = "Utilizador não existe";
    protected string _ERROR_SPMLPWDHISTORY = "Password no histórico - Deve inserir uma password diferente das duas últimas";
    protected string _ERROR_SPMLPWDVIOLATION = "Password não permitida - A password deve ter um tamanho mínimo de 6 caracters com pelo menos um caracter minúsculo e pelo menos um número";
    protected string _SESSIONTOKEN = "";
    protected string _PASSWORDUSERRESET = "";
    protected string _SPMLURLWEBSERVICE;
    protected string _SPMLUSERNAME;
    protected string _SPMLPASSWORD;
    protected string _RESULT;
    protected string _ERROR;
    protected string _SPMLERRORCOD;
    protected bool _SPMLERRORRESULT;

    public string SPMLResult
    {
      get
      {
        return this._RESULT;
      }
      set
      {
        this._RESULT = value;
      }
    }

    public string SPMLError
    {
      get
      {
        return this._ERROR;
      }
      set
      {
        this._ERROR = value;
      }
    }

    private bool SPMLErrorResult
    {
      get
      {
        return this._SPMLERRORRESULT;
      }
      set
      {
        this._SPMLERRORRESULT = value;
      }
    }

    public string SPMLErrorCod
    {
      get
      {
        return this._SPMLERRORCOD;
      }
      set
      {
        this._SPMLERRORCOD = value;
      }
    }

    public string SPMLSessionToken
    {
      get
      {
        return this._SESSIONTOKEN;
      }
      set
      {
        this._SESSIONTOKEN = value;
      }
    }

    public string SPMLNewUserPassword
    {
      get
      {
        return this._PASSWORDUSERRESET;
      }
      set
      {
        this._PASSWORDUSERRESET = value;
      }
    }

    public spmlService()
    {
    }

    public spmlService(string SPMLUrl, string SPMLUserName, string SPMLPassword)
    {
      this._SPMLURLWEBSERVICE = SPMLUrl;
      this._SPMLUSERNAME = SPMLUserName;
      this._SPMLPASSWORD = SPMLPassword;
    }

    public bool SPMLChangeUserPassword(string account, string password)
    {
      bool flag = true;
      XmlDocument xmlDocument1 = new XmlDocument();
      int num = 1;
      try
      {
        Encoding.GetEncoding("utf-8");
        ASCIIEncoding asciiEncoding = new ASCIIEncoding();
        string xml = "<soap:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/'><soap:Body><spml:extendedRequest xmlns:spml='urn:oasis:names:tc:SPML:1:0' xmlns:dsml='urn:oasis:names:tc:DSML:2:0:core'><spml:operationalAttributes><dsml:attr name='session'><dsml:value>" + this.SPMLSessionToken + "</dsml:value></dsml:attr></spml:operationalAttributes><spml:identifier type='urn:oasis:names:tc:SPML:1:0#UserIDAndOrDomainName'><spml:id>" + account + "</spml:id></spml:identifier><spml:operationIdentifier operationIDType='urn:oasis:names:tc:SPML:1:0#GenericString'><spml:operationID>changeUserPassword</spml:operationID></spml:operationIdentifier><spml:attributes><dsml:attr name='accountId'><dsml:value>" + account + "</dsml:value></dsml:attr><dsml:attr name='password'><dsml:value>" + password + "</dsml:value></dsml:attr></spml:attributes></spml:extendedRequest></soap:Body></soap:Envelope>";
        XmlDocument xmlDocument2 = new XmlDocument();
        xmlDocument2.LoadXml(xml);
        HttpWebRequest httpWebRequest = (HttpWebRequest) WebRequest.Create(this._SPMLURLWEBSERVICE);
        httpWebRequest.ContentType = "text/xml; charset=utf-8";
        httpWebRequest.Accept = "text/xml";
        httpWebRequest.Method = "POST";
        httpWebRequest.Timeout = num * 1000;
        using (Stream requestStream = httpWebRequest.GetRequestStream())
          xmlDocument2.Save(requestStream);
        StreamReader streamReader = new StreamReader(httpWebRequest.GetResponse().GetResponseStream());
        string XLMResult = streamReader.ReadToEnd();
        streamReader.Close();
        if (!this.getSPMLResult2(XLMResult))
        {
          if (!this.SPMLErrorResult)
          {
            this.SPMLError = this._ERROR_SPMLCHGUSERPWD;
            this.SPMLErrorCod = this._ERROR_SPMLCHGUSERPWD_COD;
          }
          flag = false;
        }
        else if (!this.modifyRequestPassword(account))
          flag = false;
      }
      catch (Exception ex)
      {
        this.SPMLError = this._ERROR_SPMLCHGUSERPWD;
        this.SPMLErrorCod = this._ERROR_SPMLCHGUSERPWD_COD;
        flag = false;
      }
      return flag;
    }

    public bool SPMLResetUserPassword(string account)
    {
      bool flag = true;
      XmlDocument xmlDocument1 = new XmlDocument();
      int num1 = 1;
      try
      {
        Encoding.GetEncoding("utf-8");
        ASCIIEncoding asciiEncoding = new ASCIIEncoding();
        string xml = "<soap:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/'><soap:Body><spml:extendedRequest xmlns:spml='urn:oasis:names:tc:SPML:1:0' xmlns:dsml='urn:oasis:names:tc:DSML:2:0:core'><spml:operationalAttributes><dsml:attr name='session'><dsml:value>" + this.SPMLSessionToken + "</dsml:value></dsml:attr></spml:operationalAttributes><spml:identifier type='urn:oasis:names:tc:SPML:1:0#UserIDAndOrDomainName'><spml:id>" + account + "</spml:id></spml:identifier><spml:operationIdentifier operationIDType='urn:oasis:names:tc:SPML:1:0#GenericString'><spml:operationID>resetUserPassword</spml:operationID></spml:operationIdentifier><spml:attributes><dsml:attr name='accountId'><dsml:value>" + account + "</dsml:value></dsml:attr></spml:attributes></spml:extendedRequest></soap:Body></soap:Envelope>";
        XmlDocument xmlDocument2 = new XmlDocument();
        xmlDocument2.LoadXml(xml);
        HttpWebRequest httpWebRequest = (HttpWebRequest) WebRequest.Create(this._SPMLURLWEBSERVICE);
        httpWebRequest.ContentType = "text/xml; charset=utf-8";
        httpWebRequest.Accept = "text/xml";
        httpWebRequest.Method = "POST";
        httpWebRequest.Timeout = num1 * 100000;
        using (Stream requestStream = httpWebRequest.GetRequestStream())
          xmlDocument2.Save(requestStream);
        StreamReader streamReader = new StreamReader(httpWebRequest.GetResponse().GetResponseStream());
        string XLMResult = streamReader.ReadToEnd();
        streamReader.Close();
        if (!this.getSPMLResult(XLMResult))
        {
          if (!this.SPMLErrorResult)
          {
            this.SPMLError = this._ERROR_SPMLRSTUSERPWD;
            this.SPMLErrorCod = this._ERROR_SPMLRSTUSERPWD_COD;
          }
          flag = false;
        }
        else if (!this.modifyRequestPassword(account))
        {
          flag = false;
        }
        else
        {
          int num2 = XLMResult.IndexOf("<dsml:attr name='Lighthouse'>");
          if (!num2.Equals((object) -1))
          {
            int startIndex = XLMResult.IndexOf("<dsml:value>", num2 + "<dsml:attr name='Lighthouse'>".Length);
            int num3 = XLMResult.IndexOf("</dsml:value>", startIndex);
            this.SPMLNewUserPassword = XLMResult.Substring(startIndex + "<dslm:value>".Length, num3 - (startIndex + "<dslm:value>".Length));
          }
        }
      }
      catch (Exception ex)
      {
        this.SPMLError = this._ERROR_SPMLRSTUSERPWD;
        this.SPMLErrorCod = this._ERROR_SPMLRSTUSERPWD_COD;
        flag = false;
      }
      return flag;
    }

    public bool SPMLLogin()
    {
      bool flag = true;
      XmlDocument xmlDocument1 = new XmlDocument();
      int num1 = 1;
      try
      {
        Encoding.GetEncoding("utf-8");
        ASCIIEncoding asciiEncoding = new ASCIIEncoding();
        string xml = "<s:Envelope xmlns:s='http://schemas.xmlsoap.org/soap/envelope/'><s:Body><spml:extendedRequest xmlns:spml='urn:oasis:names:tc:SPML:1:0' xmlns:dsml='urn:oasis:names:tc:DSML:2:0:core'><spml:operationIdentifier operationIDType='urn:oasis:names:tc:SPML:1:0#GenericString'><spml:operationID>login</spml:operationID></spml:operationIdentifier><spml:attributes><dsml:attr name='accountId'><dsml:value>" + this._SPMLUSERNAME + "</dsml:value></dsml:attr><dsml:attr name='password'><dsml:value>" + this._SPMLPASSWORD + "</dsml:value></dsml:attr></spml:attributes></spml:extendedRequest></s:Body></s:Envelope>";
        XmlDocument xmlDocument2 = new XmlDocument();
        xmlDocument2.LoadXml(xml);
        HttpWebRequest httpWebRequest = (HttpWebRequest) WebRequest.Create(this._SPMLURLWEBSERVICE);
        httpWebRequest.ContentType = "text/xml; charset=utf-8";
        httpWebRequest.Accept = "text/xml";
        httpWebRequest.Method = "POST";
        httpWebRequest.Timeout = num1 * 1000;
        using (Stream requestStream = httpWebRequest.GetRequestStream())
          xmlDocument2.Save(requestStream);
        StreamReader streamReader = new StreamReader(httpWebRequest.GetResponse().GetResponseStream());
        string XLMResult = streamReader.ReadToEnd();
        streamReader.Close();
        if (this.getSPMLResult(XLMResult))
        {
          int num2 = XLMResult.IndexOf("<dsml:value>");
          int num3 = XLMResult.IndexOf("</dsml:value>");
          this.SPMLSessionToken = XLMResult.Substring(num2 + "<dslm:value>".Length, num3 - (num2 + "<dslm:value>".Length));
        }
        else
        {
          this.SPMLError = this._ERROR_SPMLLOGIN;
          this.SPMLErrorCod = this._ERROR_SPMLLOGIN_COD;
          flag = false;
        }
      }
      catch (Exception ex)
      {
        this.SPMLError = this._ERROR_SPMLLOGIN;
        this.SPMLErrorCod = this._ERROR_SPMLLOGIN_COD;
        flag = false;
      }
      return flag;
    }

    public bool SPMLLogout()
    {
      bool flag = true;
      XmlDocument xmlDocument1 = new XmlDocument();
      int num = 1;
      try
      {
        Encoding.GetEncoding("utf-8");
        ASCIIEncoding asciiEncoding = new ASCIIEncoding();
        string xml = "<s:Envelope xmlns:s='http://schemas.xmlsoap.org/soap/envelope/'><s:Body><spml:extendedRequest xmlns:spml='urn:oasis:names:tc:SPML:1:0' xmlns:dsml='urn:oasis:names:tc:DSML:2:0:core'><spml:operationalAttributes><dsml:attr name='session'><dsml:value>" + this.SPMLSessionToken + "</dsml:value></dsml:attr></spml:operationalAttributes><spml:operationIdentifier operationIDType='urn:oasis:names:tc:SPML:1:0#GenericString'><spml:operationID>logout</spml:operationID></spml:operationIdentifier><spml:attributes><dsml:attr name='accountId'><dsml:value>" + this._SPMLUSERNAME + "</dsml:value></dsml:attr><dsml:attr name='password'><dsml:value>" + this._SPMLPASSWORD + "</dsml:value></dsml:attr></spml:attributes></spml:extendedRequest><spml:modifications><dsml:modification name='expirePassword' operation='replace'><dsml:value>1</dsml:value></dsml:modification><dsml:modification name='pwdLastSet' operation='replace'><dsml:value>-1</dsml:value></dsml:modification></spml:modifications></s:Body></s:Envelope>";
        XmlDocument xmlDocument2 = new XmlDocument();
        xmlDocument2.LoadXml(xml);
        HttpWebRequest httpWebRequest = (HttpWebRequest) WebRequest.Create(this._SPMLURLWEBSERVICE);
        httpWebRequest.ContentType = "text/xml; charset=utf-8";
        httpWebRequest.Accept = "text/xml";
        httpWebRequest.Method = "POST";
        httpWebRequest.Timeout = num * 1000;
        using (Stream requestStream = httpWebRequest.GetRequestStream())
          xmlDocument2.Save(requestStream);
        StreamReader streamReader = new StreamReader(httpWebRequest.GetResponse().GetResponseStream());
        string XLMResult = streamReader.ReadToEnd();
        streamReader.Close();
        if (!this.getSPMLResult(XLMResult))
        {
          if (!this.SPMLErrorResult)
          {
            this.SPMLError = this._ERROR_SPMLLOGOUT;
            this.SPMLErrorCod = this._ERROR_SPMLLOGOUT_COD;
          }
          flag = false;
        }
      }
      catch (Exception ex)
      {
        this.SPMLError = this._ERROR_SPMLLOGOUT;
        this.SPMLErrorCod = this._ERROR_SPMLLOGOUT_COD;
        flag = false;
      }
      return flag;
    }

    private bool getSPMLResult(string XLMResult)
    {
      bool flag = true;
      this.SPMLErrorResult = false;
      try
      {
        int num1 = XLMResult.IndexOf("result='urn:oasis:names:tc:SPML:1:0#");
        if (!num1.Equals((object) -1))
        {
          int num2 = XLMResult.IndexOf("'", num1 + "result='urn:oasis:names:tc:SPML:1:0#".Length);
          if (XLMResult.Substring(num1 + "result='urn:oasis:names:tc:SPML:1:0#".Length, num2 - (num1 + "result='urn:oasis:names:tc:SPML:1:0#".Length)).Equals("success"))
          {
            this.SPMLResult = "success";
          }
          else
          {
            int num3 = XLMResult.IndexOf("<spml:errorMessage>");
            if (!num3.Equals((object) -1))
            {
              int num4 = XLMResult.IndexOf("</spml:errorMessage>");
              this.SPMLError = XLMResult.Substring(num3 + "<spml:errorMessage>".Length, num4 - (num3 + "<spml:errorMessage>".Length));
              this.SPMLError = this.handleError(this.SPMLError);
              this.SPMLErrorResult = true;
            }
            this.SPMLResult = "failure";
            flag = false;
          }
        }
        else
        {
          this.SPMLResult = "failure";
          flag = false;
        }
      }
      catch
      {
        this.SPMLResult = "failure";
        flag = false;
      }
      return flag;
    }

    private bool getSPMLResult2(string XLMResult)
    {
      bool flag = true;
      this.SPMLErrorResult = false;
      try
      {
        int num1 = XLMResult.IndexOf("result='urn:oasis:names:tc:SPML:1:0#");
        if (!num1.Equals((object) -1))
        {
          int num2 = XLMResult.IndexOf("'", num1 + "result='urn:oasis:names:tc:SPML:1:0#".Length);
          if (XLMResult.Substring(num1 + "result='urn:oasis:names:tc:SPML:1:0#".Length, num2 - (num1 + "result='urn:oasis:names:tc:SPML:1:0#".Length)).Equals("success"))
          {
            this.SPMLResult = "success";
          }
          else
          {
            int num3 = XLMResult.IndexOf("<dsml:attr name='errorMessages'>");
            if (!num3.Equals((object) -1))
            {
              int num4 = XLMResult.IndexOf("</dsml:attr>");
              this.SPMLError = XLMResult.Substring(num3 + "<dsml:attr name='errorMessages'>".Length, num4 - (num3 + "<dsml:attr name='errorMessages'>".Length));
              this.SPMLError = this.handleError(this.SPMLError);
              this.SPMLErrorResult = true;
            }
            else
            {
              num3 = XLMResult.IndexOf("<spml:errorMessage>");
              if (!num3.Equals((object) -1))
              {
                int num4 = XLMResult.IndexOf("</spml:errorMessage>");
                this.SPMLError = XLMResult.Substring(num3 + "<spml:errorMessage>".Length, num4 - (num3 + "<spml:errorMessage>".Length));
                this.SPMLError = this.handleError(this.SPMLError);
                this.SPMLErrorResult = true;
              }
            }
            this.SPMLResult = "failure";
            flag = false;
          }
        }
        else
        {
          this.SPMLResult = "failure";
          flag = false;
        }
      }
      catch
      {
        this.SPMLResult = "failure";
        flag = false;
      }
      return flag;
    }

    private string handleError(string SPMLError)
    {
      string str = SPMLError;
      if (!SPMLError.IndexOf("com.waveset.exception.ItemNotFound").Equals((object) -1))
      {
        str = this._ERROR_SPMLUSERNOTEXIST;
        this.SPMLErrorCod = this._ERROR_SPMLUSERNOTEXIST_COD;
      }
      else if (!SPMLError.IndexOf("<dsml:value>[LDAP: error code 19 - password in history]</dsml:value>").Equals((object) -1))
      {
        str = this._ERROR_SPMLPWDHISTORY;
        this.SPMLErrorCod = this._ERROR_SPMLPWDHISTORY_COD;
      }
      else if (!SPMLError.IndexOf("com.waveset.exception.PolicyViolation").Equals((object) -1))
      {
        str = this._ERROR_SPMLPWDVIOLATION;
        this.SPMLErrorCod = this._ERROR_SPMLPWDVIOLATION_COD;
      }
      else
        this.SPMLErrorCod = this._ERROR_SPMLDEFAULT_COD;
      return str;
    }

    public bool modifyRequestPassword(string account)
    {
      bool flag = true;
      XmlDocument xmlDocument1 = new XmlDocument();
      int num = 1;
      try
      {
        Encoding.GetEncoding("utf-8");
        ASCIIEncoding asciiEncoding = new ASCIIEncoding();
        string xml = "<soap:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/'><soap:Body><spml:modifyRequest xmlns:spml='urn:oasis:names:tc:SPML:1:0' xmlns:dsml='urn:oasis:names:tc:DSML:2:0:core' requestID='modifyRequest'><spml:operationalAttributes><dsml:attr name='session'><dsml:value>" + this.SPMLSessionToken + "</dsml:value></dsml:attr></spml:operationalAttributes><spml:identifier type='urn:oasis:names:tc:SPML:1:0#UserIDAndOrDomainName'><spml:id>" + account + "</spml:id></spml:identifier><spml:modifications><dsml:modification name='expirePassword' operation='replace'><dsml:value>0</dsml:value></dsml:modification><dsml:modification name='pwdLastSet' operation='replace'><dsml:value>-1</dsml:value></dsml:modification></spml:modifications></spml:modifyRequest></soap:Body></soap:Envelope>";
        XmlDocument xmlDocument2 = new XmlDocument();
        xmlDocument2.LoadXml(xml);
        HttpWebRequest httpWebRequest = (HttpWebRequest) WebRequest.Create(this._SPMLURLWEBSERVICE);
        httpWebRequest.ContentType = "text/xml; charset=utf-8";
        httpWebRequest.Accept = "text/xml";
        httpWebRequest.Method = "POST";
        httpWebRequest.Timeout = num * 100000;
        using (Stream requestStream = httpWebRequest.GetRequestStream())
          xmlDocument2.Save(requestStream);
        StreamReader streamReader = new StreamReader(httpWebRequest.GetResponse().GetResponseStream());
        streamReader.ReadToEnd();
        streamReader.Close();
      }
      catch (Exception ex)
      {
        flag = false;
      }
      return flag;
    }

    public bool modifyRequestDisplayName(string account, string displayName)
    {
      bool flag = true;
      XmlDocument xmlDocument1 = new XmlDocument();
      int num = 1;
      try
      {
        Encoding.GetEncoding("utf-8");
        ASCIIEncoding asciiEncoding = new ASCIIEncoding();
        string xml = "<soap:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/'><soap:Body><spml:modifyRequest xmlns:spml='urn:oasis:names:tc:SPML:1:0' xmlns:dsml='urn:oasis:names:tc:DSML:2:0:core' requestID='modifyRequest'><spml:operationalAttributes><dsml:attr name='session'><dsml:value>" + this.SPMLSessionToken + "</dsml:value></dsml:attr></spml:operationalAttributes><spml:identifier type='urn:oasis:names:tc:SPML:1:0#UserIDAndOrDomainName'><spml:id>" + account + "</spml:id></spml:identifier><spml:modifications><dsml:modification name='staff_v_displayname' operation='replace'><dsml:value>" + displayName + "</dsml:value></dsml:modification></spml:modifications></spml:modifyRequest></soap:Body></soap:Envelope>";
        XmlDocument xmlDocument2 = new XmlDocument();
        xmlDocument2.LoadXml(xml);
        HttpWebRequest httpWebRequest = (HttpWebRequest) WebRequest.Create(this._SPMLURLWEBSERVICE);
        httpWebRequest.ContentType = "text/xml; charset=utf-8";
        httpWebRequest.Accept = "text/xml";
        httpWebRequest.Method = "POST";
        httpWebRequest.Timeout = num * 100000;
        using (Stream requestStream = httpWebRequest.GetRequestStream())
          xmlDocument2.Save(requestStream);
        StreamReader streamReader = new StreamReader(httpWebRequest.GetResponse().GetResponseStream());
        streamReader.ReadToEnd();
        streamReader.Close();
      }
      catch (Exception ex)
      {
        this.SPMLError = this._ERROR_SPMLCHGUSERDATA;
        this.SPMLErrorCod = this._ERROR_SPMLCHGUSERDATA_COD;
        flag = false;
      }
      return flag;
    }

    public enum SPLMResults
    {
      success,
      failure,
    }
  }
}
