﻿// Decompiled with JetBrains decompiler
// Type: IDMWebService.Global
// Assembly: IDMWebService, Version=1.0.3299.20037, Culture=neutral, PublicKeyToken=null
// MVID: ACEF342D-9388-4D48-87E3-3A93C0ED0718
// Assembly location: A:\Git repositories\idmwebservice\IIS\bin\IDMWebService.dll

using System;
using System.ComponentModel;
using System.Web;

namespace IDMWebService
{
  public class Global : HttpApplication
  {
    private IContainer components = (IContainer) null;

    public Global()
    {
      this.InitializeComponent();
    }

    protected void Application_Start(object sender, EventArgs e)
    {
    }

    protected void Session_Start(object sender, EventArgs e)
    {
    }

    protected void Application_BeginRequest(object sender, EventArgs e)
    {
    }

    protected void Application_EndRequest(object sender, EventArgs e)
    {
    }

    protected void Application_AuthenticateRequest(object sender, EventArgs e)
    {
    }

    protected void Application_Error(object sender, EventArgs e)
    {
    }

    protected void Session_End(object sender, EventArgs e)
    {
    }

    protected void Application_End(object sender, EventArgs e)
    {
    }

    private void InitializeComponent()
    {
      this.components = (IContainer) new Container();
    }
  }
}
