﻿// Decompiled with JetBrains decompiler
// Type: DataProtection.DataProtector
// Assembly: DataProtection, Version=1.0.1901.18205, Culture=neutral, PublicKeyToken=null
// MVID: 428204C2-6A55-424F-A4C3-B66C626ECD88
// Assembly location: A:\Git repositories\idmwebservice\IIS\bin\DataProtection.dll

using System;
using System.Runtime.InteropServices;

namespace DataProtection
{
  public class DataProtector
  {
    private static IntPtr NullPtr = (IntPtr) 0;
    private const int CRYPTPROTECT_UI_FORBIDDEN = 1;
    private const int CRYPTPROTECT_LOCAL_MACHINE = 4;
    private DataProtector.Store store;

    public DataProtector(DataProtector.Store tempStore)
    {
      this.store = tempStore;
    }

    [DllImport("Crypt32.dll", CharSet = CharSet.Auto, SetLastError = true)]
    private static extern bool CryptProtectData(ref DataProtector.DATA_BLOB pDataIn, string szDataDescr, ref DataProtector.DATA_BLOB pOptionalEntropy, IntPtr pvReserved, ref DataProtector.CRYPTPROTECT_PROMPTSTRUCT pPromptStruct, int dwFlags, ref DataProtector.DATA_BLOB pDataOut);

    [DllImport("Crypt32.dll", CharSet = CharSet.Auto, SetLastError = true)]
    private static extern bool CryptUnprotectData(ref DataProtector.DATA_BLOB pDataIn, string szDataDescr, ref DataProtector.DATA_BLOB pOptionalEntropy, IntPtr pvReserved, ref DataProtector.CRYPTPROTECT_PROMPTSTRUCT pPromptStruct, int dwFlags, ref DataProtector.DATA_BLOB pDataOut);

    [DllImport("kernel32.dll", CharSet = CharSet.Auto)]
    private static extern unsafe int FormatMessage(int dwFlags, ref IntPtr lpSource, int dwMessageId, int dwLanguageId, ref string lpBuffer, int nSize, IntPtr* Arguments);

    public byte[] Encrypt(byte[] plainText, byte[] optionalEntropy)
    {
      DataProtector.DATA_BLOB pDataIn = new DataProtector.DATA_BLOB();
      DataProtector.DATA_BLOB pDataOut = new DataProtector.DATA_BLOB();
      DataProtector.DATA_BLOB pOptionalEntropy = new DataProtector.DATA_BLOB();
      DataProtector.CRYPTPROTECT_PROMPTSTRUCT cryptprotectPromptstruct = new DataProtector.CRYPTPROTECT_PROMPTSTRUCT();
      this.InitPromptstruct(ref cryptprotectPromptstruct);
      try
      {
        try
        {
          int length = plainText.Length;
          pDataIn.pbData = Marshal.AllocHGlobal(length);
          if (IntPtr.Zero == pDataIn.pbData)
            throw new Exception("Unable to allocate plaintext buffer.");
          pDataIn.cbData = length;
          Marshal.Copy(plainText, 0, pDataIn.pbData, length);
        }
        catch (Exception ex)
        {
          throw new Exception("Exception marshalling data. " + ex.Message);
        }
        int dwFlags;
        if (DataProtector.Store.USE_MACHINE_STORE == this.store)
        {
          dwFlags = 5;
          if (optionalEntropy == null)
            optionalEntropy = new byte[0];
          try
          {
            int length = optionalEntropy.Length;
            pOptionalEntropy.pbData = Marshal.AllocHGlobal(optionalEntropy.Length);
            if (IntPtr.Zero == pOptionalEntropy.pbData)
              throw new Exception("Unable to allocate entropy data buffer.");
            Marshal.Copy(optionalEntropy, 0, pOptionalEntropy.pbData, length);
            pOptionalEntropy.cbData = length;
          }
          catch (Exception ex)
          {
            throw new Exception("Exception entropy marshalling data. " + ex.Message);
          }
        }
        else
          dwFlags = 1;
        if (!DataProtector.CryptProtectData(ref pDataIn, "", ref pOptionalEntropy, IntPtr.Zero, ref cryptprotectPromptstruct, dwFlags, ref pDataOut))
          throw new Exception("Encryption failed. " + DataProtector.GetErrorMessage(Marshal.GetLastWin32Error()));
        if (IntPtr.Zero != pDataIn.pbData)
          Marshal.FreeHGlobal(pDataIn.pbData);
        if (IntPtr.Zero != pOptionalEntropy.pbData)
          Marshal.FreeHGlobal(pOptionalEntropy.pbData);
      }
      catch (Exception ex)
      {
        throw new Exception("Exception encrypting. " + ex.Message);
      }
      byte[] destination = new byte[pDataOut.cbData];
      Marshal.Copy(pDataOut.pbData, destination, 0, pDataOut.cbData);
      Marshal.FreeHGlobal(pDataOut.pbData);
      return destination;
    }

    public byte[] Decrypt(byte[] cipherText, byte[] optionalEntropy)
    {
      DataProtector.DATA_BLOB pDataOut = new DataProtector.DATA_BLOB();
      DataProtector.DATA_BLOB pDataIn = new DataProtector.DATA_BLOB();
      DataProtector.CRYPTPROTECT_PROMPTSTRUCT cryptprotectPromptstruct = new DataProtector.CRYPTPROTECT_PROMPTSTRUCT();
      this.InitPromptstruct(ref cryptprotectPromptstruct);
      try
      {
        try
        {
          int length = cipherText.Length;
          pDataIn.pbData = Marshal.AllocHGlobal(length);
          if (IntPtr.Zero == pDataIn.pbData)
            throw new Exception("Unable to allocate cipherText buffer.");
          pDataIn.cbData = length;
          Marshal.Copy(cipherText, 0, pDataIn.pbData, pDataIn.cbData);
        }
        catch (Exception ex)
        {
          throw new Exception("Exception marshalling data. " + ex.Message);
        }
        DataProtector.DATA_BLOB pOptionalEntropy = new DataProtector.DATA_BLOB();
        int dwFlags;
        if (DataProtector.Store.USE_MACHINE_STORE == this.store)
        {
          dwFlags = 5;
          if (optionalEntropy == null)
            optionalEntropy = new byte[0];
          try
          {
            int length = optionalEntropy.Length;
            pOptionalEntropy.pbData = Marshal.AllocHGlobal(length);
            if (IntPtr.Zero == pOptionalEntropy.pbData)
              throw new Exception("Unable to allocate entropy buffer.");
            pOptionalEntropy.cbData = length;
            Marshal.Copy(optionalEntropy, 0, pOptionalEntropy.pbData, length);
          }
          catch (Exception ex)
          {
            throw new Exception("Exception entropy marshalling data. " + ex.Message);
          }
        }
        else
          dwFlags = 1;
        if (!DataProtector.CryptUnprotectData(ref pDataIn, (string) null, ref pOptionalEntropy, IntPtr.Zero, ref cryptprotectPromptstruct, dwFlags, ref pDataOut))
          throw new Exception("Decryption failed. " + DataProtector.GetErrorMessage(Marshal.GetLastWin32Error()));
        if (IntPtr.Zero != pDataIn.pbData)
          Marshal.FreeHGlobal(pDataIn.pbData);
        if (IntPtr.Zero != pOptionalEntropy.pbData)
          Marshal.FreeHGlobal(pOptionalEntropy.pbData);
      }
      catch (Exception ex)
      {
        throw new Exception("Exception decrypting. " + ex.Message);
      }
      byte[] destination = new byte[pDataOut.cbData];
      Marshal.Copy(pDataOut.pbData, destination, 0, pDataOut.cbData);
      Marshal.FreeHGlobal(pDataOut.pbData);
      return destination;
    }

    private void InitPromptstruct(ref DataProtector.CRYPTPROTECT_PROMPTSTRUCT ps)
    {
      ps.cbSize = Marshal.SizeOf(typeof (DataProtector.CRYPTPROTECT_PROMPTSTRUCT));
      ps.dwPromptFlags = 0;
      ps.hwndApp = DataProtector.NullPtr;
      ps.szPrompt = (string) null;
    }

    private static unsafe string GetErrorMessage(int errorCode)
    {
      int num1 = 256;
      int num2 = 512;
      int num3 = 4096;
      int nSize = (int) byte.MaxValue;
      string lpBuffer = "";
      int dwFlags = num1 | num3 | num2;
      IntPtr lpSource = new IntPtr();
      IntPtr num4 = new IntPtr();
      if (DataProtector.FormatMessage(dwFlags, ref lpSource, errorCode, 0, ref lpBuffer, nSize, &num4) == 0)
        throw new Exception("Failed to format message for error code " + (object) errorCode + ". ");
      return lpBuffer;
    }

    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
    internal struct DATA_BLOB
    {
      public int cbData;
      public IntPtr pbData;
    }

    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
    internal struct CRYPTPROTECT_PROMPTSTRUCT
    {
      public int cbSize;
      public int dwPromptFlags;
      public IntPtr hwndApp;
      public string szPrompt;
    }

    public enum Store
    {
      USE_MACHINE_STORE = 1,
      USE_USER_STORE = 2,
    }
  }
}
