﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web;

namespace IDMWebService
{
    public static class WebConfigValues
    {
        public static int Timeout
        {
            get
            {
                int ret = 10;
                if(int.TryParse(ConfigurationManager.AppSettings["SPML.Timeout"], out ret))
                    return ret;
                return ret;
            }
        }
    }
}